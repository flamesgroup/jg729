# jg729

Java library to encode and decode [G729](https://en.wikipedia.org/wiki/G.729) audio codec from and to [PCM](https://en.wikipedia.org/wiki/Pulse-code_modulation).

## Overview

**jg729** implements JNI wrappers over [BCG729 library](http://www.linphone.org/technical-corner/bcg729/overview).

## Structure

* `java` - maven project with java implementation of JNI wrappers
* `native` - maven project with native implementation of JNI wrappers
* `build-jni-heade` - script to generate JNI header files from java classes

## Build

### Build bcg729

For proper build **jg729**, [BCG729 library](http://www.linphone.org/technical-corner/bcg729/overview) must be cloned, configured, compiled and installed:
```
$ git clone git://git.linphone.org/bcg729.git
$ cd bcg729
$ ./configure --with-pic
$ make
$ make install
```
`--with-pic` configure parameter is mandatory for correct static linking.

If [BCG729 library](http://www.linphone.org/technical-corner/bcg729/overview) was configured with some `--prefix` then for errorless compilation next environment variables must be declared:
```
$ export CPATH=<PREFIX>/include/
$ export LIBRARY_PATH=<PREFIX>/lib/
```

### Build native code

To build native library for current platform (for now AMD64 and ARM supported) in `native` directory execute next command:
```
$ mvn install
```

### Build java code

To build and install java library with native library as resource in project main directory execute next command:
```
$mvn install
```

## Usage examples

```java
import com.flamesgroup.jg729.IG729;
import com.flamesgroup.jg729.IG729Decoder;
import com.flamesgroup.jg729.IG729Encoder;
import com.flamesgroup.jg729.jni.G729;

public class G729Exmaple {

  public static void main(String[] args) throws Exception {
    IG729 g729 = new G729();

    IG729Decoder g729Decoder = g729.createDecoder();
    g729Decoder.decode(...);

    IG729Encoder g729Encoder = g729.createEncoder();
    g729Encoder.encode(...);
  }

}
```

## Questions

If you have any questions about **jg729**, feel free to create issue with it.
