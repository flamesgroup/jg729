/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.jg729;

import java.nio.ByteBuffer;
import java.nio.ShortBuffer;

public interface IG729Decoder {

  int decode(byte[] data, int dataOffset, int dataLength, byte[] pcm, int pcmOffset, int pcmFrameSize) throws G729DecoderException;

  int decode(byte[] data, int dataOffset, int dataLength, short[] pcm, int pcmOffset, int pcmFrameSize) throws G729DecoderException;

  void decode(ByteBuffer data, ShortBuffer pcm) throws G729DecoderException;

  int decode(byte[] data, int dataOffset, int dataLength, boolean frameErasureFlag, byte[] pcm, int pcmOffset, int pcmFrameSize) throws G729DecoderException;

  int decode(byte[] data, int dataOffset, int dataLength, boolean frameErasureFlag, short[] pcm, int pcmOffset, int pcmFrameSize) throws G729DecoderException;

  void decode(ByteBuffer data, boolean frameErasureFlag, ShortBuffer pcm) throws G729DecoderException;

}
