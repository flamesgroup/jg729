/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.jg729.jni;

import com.flamesgroup.jg729.G729DecoderException;
import com.flamesgroup.jg729.G729EncoderException;
import com.flamesgroup.jg729.IG729;
import com.flamesgroup.jg729.IG729Decoder;
import com.flamesgroup.jg729.IG729Encoder;
import org.scijava.nativelib.NativeLibraryUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class G729 implements IG729 {

  private static final Logger logger = LoggerFactory.getLogger(G729.class);

  private static final boolean isLoadNativeLibrarySuccess;

  private static native long EncoderCreate() throws G729EncoderException;

  private static native long DecoderCreate() throws G729DecoderException;

  static {
    String libraryName = "g729a-native";

    isLoadNativeLibrarySuccess = NativeLibraryUtil.loadNativeLibrary(G729.class, libraryName);
  }

  public G729() {
    super();
    if (!isLoadNativeLibrarySuccess) {
      throw new UnsupportedOperationException("native library isn't loaded");
    }
  }

  @Override
  public IG729Decoder createDecoder() throws G729DecoderException {
    long decoder = DecoderCreate();
    if (decoder == 0L) {
      throw new G729DecoderException("[" + this + "] - failed to allocate Decoder");
    } else {
      IG729Decoder g729Decoder = new G729Decoder(decoder);
      logger.trace("[{}] - created [{}]", this, g729Decoder);
      return g729Decoder;
    }
  }

  @Override
  public IG729Encoder createEncoder() throws G729EncoderException {
    long encoder = EncoderCreate();
    if (encoder == 0L) {
      throw new G729EncoderException("[" + this + "] - failed to allocate Encoder");
    } else {
      IG729Encoder g729Encoder = new G729Encoder(encoder);
      logger.trace("[{}] - created [{}]", this, g729Encoder);
      return g729Encoder;
    }
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + "@" + Integer.toHexString(hashCode());
  }

}
