/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.jg729.jni;

import com.flamesgroup.jg729.G729EncoderException;
import com.flamesgroup.jg729.IG729Encoder;

import java.nio.ByteBuffer;
import java.nio.ShortBuffer;
import java.util.Objects;

public class G729Encoder implements IG729Encoder {

  private static native int Encode(long encoder, byte[] pcm, int pcmOffset, int pcmFrameSize, byte[] data, int dataOffset, int dataLength) throws G729EncoderException;

  private static native int Encode(long encoder, short[] pcm, int pcmOffset, int pcmFrameSize, byte[] data, int dataOffset, int dataLength) throws G729EncoderException;

  private static native int Encode(long encoder, ShortBuffer pcm, int pcmOffset, int pcmFrameSize, ByteBuffer data, int dataOffset, int dataLength) throws G729EncoderException;

  private static native void EncoderDestroy(long encoder);

  private long encoder = -1;

  G729Encoder(final long encoder) throws G729EncoderException {
    if (encoder == -1) {
      throw new G729EncoderException(String.format("[%s] - wasn't created encoder", this));
    }
    this.encoder = encoder;
  }

  @Override
  public int encode(final byte[] pcm, final int pcmOffset, final int pcmFrameSize, final byte[] data, final int dataOffset, final int dataLength) throws G729EncoderException {
    Objects.requireNonNull(pcm, "pcm mustn't be null");
    Objects.requireNonNull(data, "data mustn't be null");

    if (pcmOffset < 0) {
      throw new IllegalArgumentException("[" + this + "] - incorrect pcmOffset [" + pcmOffset + "]");
    }
    if (pcmFrameSize % G729Utils.PCM_BYTE_FRAME_SIZE != 0) {
      throw new IllegalArgumentException("[" + this + "] - incorrect pcmFrameSize [" + pcmFrameSize + "]");
    }
    if (dataOffset < 0) {
      throw new IllegalArgumentException("[" + this + "] - incorrect dataOffset [" + dataOffset + "]");
    }
    if (dataLength % G729Utils.G729_FRAME_SIZE != 0) {
      throw new IllegalArgumentException("[" + this + "] - incorrect dataLength [" + dataLength + "]");
    }

    if (pcmFrameSize / G729Utils.PCM_BYTE_FRAME_SIZE > dataLength / G729Utils.G729_FRAME_SIZE) {
      throw new IllegalArgumentException("[" + this + "] - incorrect correlation between pcmFrameSize [" + pcmFrameSize + "] and dataLength [" + dataLength + "]");
    }

    return Encode(encoder, pcm, pcmOffset, pcmFrameSize, data, dataOffset, dataLength);
  }

  @Override
  public int encode(final short[] pcm, final int pcmOffset, final int pcmFrameSize, final byte[] data, final int dataOffset, final int dataLength) throws G729EncoderException {
    Objects.requireNonNull(pcm, "pcm mustn't be null");
    Objects.requireNonNull(data, "data mustn't be null");

    if (pcmOffset < 0) {
      throw new IllegalArgumentException("[" + this + "] - incorrect pcmOffset [" + pcmOffset + "]");
    }
    if (pcmFrameSize % G729Utils.PCM_SHORT_FRAME_SIZE != 0) {
      throw new IllegalArgumentException("[" + this + "] - incorrect pcmFrameSize [" + pcmFrameSize + "]");
    }
    if (dataOffset < 0) {
      throw new IllegalArgumentException("[" + this + "] - incorrect dataOffset [" + dataOffset + "]");
    }
    if (dataLength % G729Utils.G729_FRAME_SIZE != 0) {
      throw new IllegalArgumentException("[" + this + "] - incorrect dataLength [" + dataLength + "]");
    }

    if (pcmFrameSize / G729Utils.PCM_SHORT_FRAME_SIZE > dataLength / G729Utils.G729_FRAME_SIZE) {
      throw new IllegalArgumentException("[" + this + "] - incorrect correlation between pcmFrameSize [" + pcmFrameSize + "] and dataLength [" + dataLength + "]");
    }

    return Encode(encoder, pcm, pcmOffset, pcmFrameSize, data, dataOffset, dataLength);
  }

  @Override
  public void encode(final ShortBuffer pcm, final ByteBuffer data) throws G729EncoderException {
    Objects.requireNonNull(pcm, "pcm mustn't be null");
    Objects.requireNonNull(data, "data mustn't be null");

    if (pcm.remaining() % G729Utils.PCM_SHORT_FRAME_SIZE != 0) {
      throw new IllegalArgumentException("[" + this + "] - incorrect pcm remaining [" + pcm.remaining() + "]");
    }
    if (data.remaining() % G729Utils.G729_FRAME_SIZE != 0) {
      throw new IllegalArgumentException("[" + this + "] - incorrect data remaining [" + data.remaining() + "]");
    }

    if (pcm.remaining() / G729Utils.PCM_SHORT_FRAME_SIZE > data.remaining() / G729Utils.G729_FRAME_SIZE) {
      throw new IllegalArgumentException("[" + this + "] - incorrect correlation between pcm remaining [" + pcm.remaining() + "] and data remaining [" + data.remaining() + "]");
    }

    int res = Encode(encoder, pcm, pcm.position(), pcm.remaining(), data, data.position(), data.remaining());

    pcm.position(pcm.limit());
    data.position(data.position() + res);
  }

  @Override
  protected void finalize() throws Throwable {
    super.finalize();
    EncoderDestroy(encoder);
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + "@" + Integer.toHexString(hashCode()) + "[" + Long.toHexString(encoder) + "]";
  }

}
