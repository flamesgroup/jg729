/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.jg729.jni;

import com.flamesgroup.jg729.G729DecoderException;
import com.flamesgroup.jg729.IG729Decoder;

import java.nio.ByteBuffer;
import java.nio.ShortBuffer;
import java.util.Objects;

public class G729Decoder implements IG729Decoder {

  private static native int Decode(long decoder, byte[] data, int dataOffset, int dataLength, boolean frameErasureFlag, byte[] pcm, int pcmOffset, int pcmFrameSize) throws G729DecoderException;

  private static native int Decode(long decoder, byte[] data, int dataOffset, int dataLength, boolean frameErasureFlag, short[] pcm, int pcmOffset, int pcmFrameSize) throws G729DecoderException;

  private static native int Decode(long decoder, ByteBuffer data, int dataOffset, int dataLength, boolean frameErasureFlag, ShortBuffer pcm, int pcmOffset, int pcmFrameSize)
      throws G729DecoderException;

  private static native void DecoderDestroy(long decoder);

  private long decoder = -1;

  G729Decoder(final long decoder) throws G729DecoderException {
    if (decoder == -1) {
      throw new G729DecoderException(String.format("[%s] - wasn't created decoder", this));
    }
    this.decoder = decoder;
  }

  @Override
  public int decode(final byte[] data, final int dataOffset, final int dataLength, final byte[] pcm, final int pcmOffset, final int pcmFrameSize) throws G729DecoderException {
    return decode(data, dataOffset, dataLength, false, pcm, pcmOffset, pcmFrameSize);
  }

  @Override
  public int decode(final byte[] data, final int dataOffset, final int dataLength, final short[] pcm, final int pcmOffset, final int pcmFrameSize) throws G729DecoderException {
    return decode(data, dataOffset, dataLength, false, pcm, pcmOffset, pcmFrameSize);
  }

  @Override
  public void decode(final ByteBuffer data, final ShortBuffer pcm) throws G729DecoderException {
    decode(data, false, pcm);
  }

  @Override
  public int decode(final byte[] data, final int dataOffset, final int dataLength, final boolean frameErasureFlag, final byte[] pcm, final int pcmOffset, final int pcmFrameSize)
      throws G729DecoderException {
    Objects.requireNonNull(data, "data mustn't be null");
    Objects.requireNonNull(pcm, "pcm mustn't be null");

    if (dataOffset < 0) {
      throw new IllegalArgumentException("[" + this + "] - incorrect dataOffset [" + dataOffset + "]");
    }
    if (dataLength % G729Utils.G729_FRAME_SIZE != 0) {
      throw new IllegalArgumentException("[" + this + "] - incorrect dataLength [" + dataLength + "]");
    }
    if (pcmOffset < 0) {
      throw new IllegalArgumentException("[" + this + "] - incorrect pcmOffset [" + pcmOffset + "]");
    }
    if (pcmFrameSize % G729Utils.PCM_BYTE_FRAME_SIZE != 0) {
      throw new IllegalArgumentException("[" + this + "] - incorrect pcmFrameSize [" + pcmFrameSize + "]");
    }

    if (dataLength / G729Utils.G729_FRAME_SIZE > pcmFrameSize / G729Utils.PCM_BYTE_FRAME_SIZE) {
      throw new IllegalArgumentException("[" + this + "] - incorrect correlation between dataLength [" + dataLength + "] and pcmFrameSize [" + pcmFrameSize + "]");
    }

    return Decode(decoder, data, dataOffset, dataLength, frameErasureFlag, pcm, pcmOffset, pcmFrameSize);
  }

  @Override
  public int decode(final byte[] data, final int dataOffset, final int dataLength, final boolean frameErasureFlag, final short[] pcm, final int pcmOffset, final int pcmFrameSize)
      throws G729DecoderException {
    Objects.requireNonNull(data, "data mustn't be null");
    Objects.requireNonNull(pcm, "pcm mustn't be null");

    if (dataOffset < 0) {
      throw new IllegalArgumentException("[" + this + "] - incorrect dataOffset [" + dataOffset + "]");
    }
    if (dataLength % G729Utils.G729_FRAME_SIZE != 0) {
      throw new IllegalArgumentException("[" + this + "] - incorrect dataLength [" + dataLength + "]");
    }
    if (pcmOffset < 0) {
      throw new IllegalArgumentException("[" + this + "] - incorrect pcmOffset [" + pcmOffset + "]");
    }
    if (pcmFrameSize % G729Utils.PCM_SHORT_FRAME_SIZE != 0) {
      throw new IllegalArgumentException("[" + this + "] - incorrect pcmFrameSize [" + pcmFrameSize + "]");
    }

    if (dataLength / G729Utils.G729_FRAME_SIZE > pcmFrameSize / G729Utils.PCM_SHORT_FRAME_SIZE) {
      throw new IllegalArgumentException("[" + this + "] - incorrect correlation between dataLength [" + dataLength + "] and pcmFrameSize [" + pcmFrameSize + "]");
    }

    return Decode(decoder, data, dataOffset, dataLength, frameErasureFlag, pcm, pcmOffset, pcmFrameSize);
  }

  @Override
  public void decode(final ByteBuffer data, final boolean frameErasureFlag, final ShortBuffer pcm) throws G729DecoderException {
    Objects.requireNonNull(data, "data mustn't be null");
    Objects.requireNonNull(pcm, "pcm mustn't be null");

    if (data.remaining() % G729Utils.G729_FRAME_SIZE != 0) {
      throw new IllegalArgumentException("[" + this + "] - incorrect data remaining [" + data.remaining() + "]");
    }
    if (pcm.remaining() % G729Utils.PCM_SHORT_FRAME_SIZE != 0) {
      throw new IllegalArgumentException("[" + this + "] - incorrect pcm remaining [" + pcm.remaining() + "]");
    }

    if (data.remaining() / G729Utils.G729_FRAME_SIZE > pcm.remaining() / G729Utils.PCM_SHORT_FRAME_SIZE) {
      throw new IllegalArgumentException("[" + this + "] - incorrect correlation between data remaining [" + data.remaining() + "] and pcm remaining [" + pcm.remaining() + "]");
    }

    int res = Decode(decoder, data, data.position(), data.remaining(), frameErasureFlag, pcm, pcm.position(), pcm.remaining());

    data.position(data.limit());
    pcm.position(pcm.position() + res);
  }

  @Override
  protected void finalize() throws Throwable {
    super.finalize();
    DecoderDestroy(decoder);
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + "@" + Integer.toHexString(hashCode()) + "[" + Long.toHexString(decoder) + "]";
  }

}
