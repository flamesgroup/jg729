/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.jg729;

import java.nio.ByteBuffer;
import java.nio.ShortBuffer;

public interface IG729Encoder {

  int encode(byte[] pcm, int pcmOffset, int pcmFrameSize, byte[] data, int dataOffset, int dataLength) throws G729EncoderException;

  int encode(short[] pcm, int pcmOffset, int pcmFrameSize, byte[] data, int dataOffset, int dataLength) throws G729EncoderException;

  void encode(ShortBuffer pcm, ByteBuffer data) throws G729EncoderException;

}
