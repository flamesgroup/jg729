/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "bcg729/encoder.h"
#include "bcg729/decoder.h"
#include "com_flamesgroup_jg729_jni_G729.h"
#include "com_flamesgroup_jg729_jni_G729Decoder.h"
#include "com_flamesgroup_jg729_jni_G729Encoder.h"
#include "helper.h"
#include <stdint.h>

#define  PCM_SHORT_FRAME_SIZE       80
#define  PCM_BYTE_FRAME_SIZE        160
#define  G729_FRAME_SIZE            10

JNIEXPORT jlong JNICALL Java_com_flamesgroup_jg729_jni_G729_EncoderCreate
(JNIEnv *env, jclass clazz) {
  bcg729EncoderChannelContextStruct *encoder = initBcg729EncoderChannel();
  return (jlong) (intptr_t) encoder;
}

JNIEXPORT jlong JNICALL Java_com_flamesgroup_jg729_jni_G729_DecoderCreate
(JNIEnv * env, jclass clazz) {
  bcg729DecoderChannelContextStruct *decoder = initBcg729DecoderChannel();
  return (jlong) (intptr_t) decoder;
}

JNIEXPORT jint JNICALL Java_com_flamesgroup_jg729_jni_G729Encoder_Encode__J_3BII_3BII
(JNIEnv *env, jclass clazz, jlong encoder, jbyteArray pcmByteArray, jint pcmOffset, jint pcmFrameSize, jbyteArray dataByteArray, jint dataOffset, jint dataLength) {
  int frame_count = pcmFrameSize / PCM_BYTE_FRAME_SIZE;
  int ret = -1;

  jbyte *pcmByte_ptr = (*env)->GetByteArrayElements(env, pcmByteArray, NULL);
  jbyte *dataByte_ptr = (*env)->GetByteArrayElements(env, dataByteArray, NULL);
  if (!pcmByte_ptr || !dataByte_ptr) {
    throwG729EncoderException(env, "can't get pointer to the array elements of pcm or data");
  } else {
    for (int n = 0; n < frame_count; n++) {
      int16_t *pcm_ptr = (int16_t *) (pcmByte_ptr + pcmOffset + PCM_BYTE_FRAME_SIZE * n);
      uint8_t *data_ptr = (uint8_t *) (dataByte_ptr + dataOffset + G729_FRAME_SIZE * n);
      bcg729Encoder((bcg729EncoderChannelContextStruct *) (intptr_t) encoder, pcm_ptr, data_ptr);
      (*env)->SetByteArrayRegion(env, dataByteArray, dataOffset + G729_FRAME_SIZE * n, G729_FRAME_SIZE, data_ptr);
    }
    ret = G729_FRAME_SIZE * frame_count;
  }
  if (pcmByte_ptr) {
    (*env)->ReleaseByteArrayElements(env, pcmByteArray, pcmByte_ptr, JNI_ABORT);
  }
  if (dataByte_ptr) {
    (*env)->ReleaseByteArrayElements(env, dataByteArray, dataByte_ptr, JNI_ABORT);
  }
  return (jint) ret;
}

JNIEXPORT jint JNICALL Java_com_flamesgroup_jg729_jni_G729Decoder_Decode__J_3BIIZ_3BII
(JNIEnv *env, jclass clazz, jlong decoder, jbyteArray dataByteArray, jint dataOffset, jint dataLength, jboolean frameErasureFlag, jbyteArray pcmByteArray, jint pcmOffset, jint pcmFrameSize) {
  int frame_count = dataLength / G729_FRAME_SIZE;
  int ret = -1;

  jbyte *dataByte_ptr = (*env)->GetByteArrayElements(env, dataByteArray, NULL);
  jbyte *pcmByte_ptr = (*env)->GetByteArrayElements(env, pcmByteArray, NULL);
  if (!dataByte_ptr || !pcmByte_ptr) {
    throwG729DecoderException(env, "can't get pointer to the array elements of pcm or data");
  } else {
    for (int n = 0; n < frame_count; n++) {
      uint8_t *data_ptr = (uint8_t *) (dataByte_ptr + dataOffset + G729_FRAME_SIZE * n);
      int16_t *pcm_ptr = (int16_t *) (pcmByte_ptr + pcmOffset + PCM_BYTE_FRAME_SIZE * n);
      bcg729Decoder((bcg729DecoderChannelContextStruct *) (intptr_t) decoder, data_ptr, 0, pcm_ptr);
      (*env)->SetByteArrayRegion(env, pcmByteArray, pcmOffset + PCM_BYTE_FRAME_SIZE * n, PCM_BYTE_FRAME_SIZE, (jbyte *) pcm_ptr);
    }
    ret = PCM_BYTE_FRAME_SIZE * frame_count;
  }
  if (dataByte_ptr) {
    (*env)->ReleaseByteArrayElements(env, dataByteArray, dataByte_ptr, JNI_ABORT);
  }
  if (pcmByte_ptr) {
    (*env)->ReleaseByteArrayElements(env, pcmByteArray, pcmByte_ptr, JNI_ABORT);
  }
  return (jint) ret;
}

JNIEXPORT jint JNICALL Java_com_flamesgroup_jg729_jni_G729Encoder_Encode__J_3SII_3BII
(JNIEnv *env, jclass clazz, jlong encoder, jshortArray pcmShortArray, jint pcmOffset, jint pcmFrameSize, jbyteArray dataByteArray, jint dataOffset, jint dataLength) {
  int frame_count = pcmFrameSize / PCM_SHORT_FRAME_SIZE;
  int ret = -1;

  jshort *pcmShort_ptr = (*env)->GetShortArrayElements(env, pcmShortArray, NULL);
  jbyte *dataByte_ptr = (*env)->GetByteArrayElements(env, dataByteArray, NULL);
  if (!pcmShort_ptr || !dataByte_ptr) {
    throwG729EncoderException(env, "can't get pointer to the array elements of pcm or data");
  } else {
    for (int n = 0; n < frame_count; n++) {
      int16_t *pcm_ptr = (int16_t *) (pcmShort_ptr + pcmOffset + PCM_SHORT_FRAME_SIZE * n);
      uint8_t *data_ptr = (uint8_t *) (dataByte_ptr + dataOffset + G729_FRAME_SIZE * n);
      bcg729Encoder((bcg729EncoderChannelContextStruct *) (intptr_t) encoder, pcm_ptr, data_ptr);
      (*env)->SetByteArrayRegion(env, dataByteArray, dataOffset + G729_FRAME_SIZE * n, G729_FRAME_SIZE, data_ptr);
    }
    ret = G729_FRAME_SIZE * frame_count;
  }
  if (pcmShort_ptr) {
    (*env)->ReleaseShortArrayElements(env, pcmShortArray, pcmShort_ptr, JNI_ABORT);
  }
  if (dataByte_ptr) {
    (*env)->ReleaseByteArrayElements(env, dataByteArray, dataByte_ptr, 0);
  }
  return (jint) ret;
}

JNIEXPORT jint JNICALL Java_com_flamesgroup_jg729_jni_G729Decoder_Decode__J_3BIIZ_3SII
(JNIEnv *env, jclass clazz, jlong decoder, jbyteArray dataByteArray, jint dataOffset, jint dataLength, jboolean frameErasureFlag, jshortArray pcmShortArray, jint pcmOffset, jint pcmFrameSize) {
  int frame_count = dataLength / G729_FRAME_SIZE;
  int ret = -1;

  jbyte *dataByte_ptr = (*env)->GetByteArrayElements(env, dataByteArray, NULL);
  jshort *pcmShort_ptr = (*env)->GetShortArrayElements(env, pcmShortArray, NULL);
  if (!dataByte_ptr || !pcmShort_ptr) {
    throwG729DecoderException(env, "can't get pointer to the array elements of pcm or data");
  } else {
    for (int n = 0; n < frame_count; n++) {
      int16_t *pcm_ptr = (int16_t *) (pcmShort_ptr + pcmOffset + PCM_SHORT_FRAME_SIZE * n);
      uint8_t *data_ptr = (uint8_t *) (dataByte_ptr + dataOffset + G729_FRAME_SIZE * n);
      bcg729Decoder((bcg729DecoderChannelContextStruct *) (intptr_t) decoder, data_ptr, frameErasureFlag, pcm_ptr);
      (*env)->SetShortArrayRegion(env, pcmShortArray, pcmOffset + PCM_SHORT_FRAME_SIZE * n, PCM_SHORT_FRAME_SIZE, pcm_ptr);
    }
    ret = PCM_SHORT_FRAME_SIZE * frame_count;
  }
  if (dataByte_ptr) {
    (*env)->ReleaseByteArrayElements(env, dataByteArray, dataByte_ptr, JNI_ABORT);
  }
  if (pcmShort_ptr) {
    (*env)->ReleaseShortArrayElements(env, pcmShortArray, pcmShort_ptr, 0);
  }
  return (jint) ret;
}

JNIEXPORT jint JNICALL Java_com_flamesgroup_jg729_jni_G729Encoder_Encode__JLjava_nio_ShortBuffer_2IILjava_nio_ByteBuffer_2II
(JNIEnv *env, jclass clazz, jlong encoder, jobject pcmDirectSB, jint pcmOffset, jint pcmFrameSize, jobject dataDirectBB, jint dataOffset, jint dataLength) {
  int frame_count = pcmFrameSize / PCM_SHORT_FRAME_SIZE;
  int ret = -1;
  
  int16_t *pcm_ptr = (int16_t *) (*env)->GetDirectBufferAddress(env, pcmDirectSB);
  if (pcm_ptr == NULL) {
    throwIllegalArgumentException(env, "memory region is undefined or the given object is not a direct java.nio.Buffer for pcm");
    return (jint) ret;
  }
  uint8_t *data_ptr = (uint8_t *) (*env)->GetDirectBufferAddress(env, dataDirectBB);
  if (data_ptr == NULL) {
    throwIllegalArgumentException(env, "memory region is undefined or the given object is not a direct java.nio.Buffer for data");
    return (jint) ret;
  }
  for (int n = 0; n < frame_count; n++) {
    pcm_ptr = pcm_ptr + pcmOffset + PCM_SHORT_FRAME_SIZE * n;
    data_ptr = data_ptr + dataOffset + G729_FRAME_SIZE * n;
    bcg729Encoder((bcg729EncoderChannelContextStruct *) (intptr_t) encoder, pcm_ptr, data_ptr);
  }
  return (jint) G729_FRAME_SIZE * frame_count;
}

JNIEXPORT jint JNICALL Java_com_flamesgroup_jg729_jni_G729Decoder_Decode__JLjava_nio_ByteBuffer_2IIZLjava_nio_ShortBuffer_2II
(JNIEnv *env, jclass clazz, jlong decoder, jobject dataDirectBB, jint dataOffset, jint dataLength, jboolean frameErasureFlag, jobject pcmDirectSB, jint pcmOffset, jint pcmFrameSize) {
  int frame_count = dataLength / G729_FRAME_SIZE; 
  int ret = -1;

  uint8_t *data_ptr = (uint8_t *) (*env)->GetDirectBufferAddress(env, dataDirectBB);
  if (data_ptr == NULL) {
    throwIllegalArgumentException(env, "memory region is undefined or the given object is not a direct java.nio.Buffer for data");
    return (jint) ret;
  }
  int16_t *pcm_ptr = (int16_t *) (*env)->GetDirectBufferAddress(env, pcmDirectSB);
  if (pcm_ptr == NULL) {
    throwIllegalArgumentException(env, "memory region is undefined or the given object is not a direct java.nio.Buffer for pcm");
    return (jint) ret;
  }
  for (int n = 0; n < frame_count; n++) {
    data_ptr += dataOffset + G729_FRAME_SIZE * n;
    pcm_ptr += pcmOffset + PCM_SHORT_FRAME_SIZE * n;
    bcg729Decoder((bcg729DecoderChannelContextStruct *) (intptr_t) decoder, data_ptr, frameErasureFlag, pcm_ptr);
  }
  return (jint) PCM_SHORT_FRAME_SIZE * frame_count;
}

JNIEXPORT void JNICALL Java_com_flamesgroup_jg729_jni_G729Encoder_EncoderDestroy
(JNIEnv *env, jclass clazz, jlong encoder) {

  closeBcg729EncoderChannel((bcg729EncoderChannelContextStruct *) (intptr_t) encoder);
}

JNIEXPORT void JNICALL Java_com_flamesgroup_jg729_jni_G729Decoder_DecoderDestroy
(JNIEnv *env, jclass class, jlong decoder) {

  closeBcg729DecoderChannel((bcg729DecoderChannelContextStruct *) (intptr_t) decoder);
}
